import DiscourseRoute from 'discourse/routes/discourse'


export default DiscourseRoute.extend({
    showFooter: true,
    renderTemplate() {
        // Renders the template `../templates/regras.hbs`
        this.render('regras');
    }
});