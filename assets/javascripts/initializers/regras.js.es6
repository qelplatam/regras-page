import { withPluginApi } from "discourse/lib/plugin-api";
const h = require("virtual-dom").h;

function initializePlugin(api) {
    api.createWidget("regras-table-of-contents-widget", {
        tagName: "div.regras-table-of-contents",
        init() {
            Ember.run.scheduleOnce('afterRender', this, () => {
                let toc = document.querySelector("ol.table-of-contents");
                let items = document.querySelectorAll("h2.title");
                items.forEach(elem => {
                    let li = document.createElement("li")
                    li.classList.add(elem.id);
                    li.innerText = elem.innerText;
                    li.addEventListener("click", e=>{
                        let tgt = document.querySelector(`h2[id=${e.currentTarget.classList[0]}]`);
                        console.log(tgt);
                        tgt.scrollIntoView(
                            {behavior: "smooth", block: "start", inline: "nearest"}
                        )
                    });
                    toc.append(li);
                })
            });
        },
        html() {
            return h("ol.table-of-contents");
        }
    });

}

export default {
    name: 'regras',
    initialize(container) {
        withPluginApi('0.1', api => initializePlugin(api));

    }
}