# name: regras
# version: 0.1.1


register_asset "stylesheets/common/regras.scss";

enabled_site_setting :regras_enabled

after_initialize do
    load File.expand_path('../app/controllers/regras_controller.rb', __FILE__)

    Discourse::Application.routes.append do
        get '/regras' => 'regras#index'
    end
end